const AppUrl = 'http://localhost:9000/api/';
window.AppUrl = AppUrl;

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import Swal from 'sweetalert2';

import App from './App.vue';


Vue.use(VueRouter);
Vue.use(VueAxios,axios);


window.Swal = Swal;

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });
  window.Toast = Toast;



import routes from './Routes/router.js';


// Vue.component('example-component', require('./components/ExampleComponent.vue').default);


const router = new VueRouter({mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({router}, App)).$mount('#app');
