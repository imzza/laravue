import Vue from 'Vue';

import VueRouter from 'vue-router';

import HomeComponent from '../components/HomeComponent.vue';
import CreateComponent from '../components/CreateComponent.vue';
import IndexComponent from '../components/IndexComponent.vue';
import EditComponent from '../components/EditComponent.vue';

// Vue.component('nav-component', require('./components/NavComponent.vue'));


const routes = [
    {
        name: 'home',
        path: '/',
        component: HomeComponent
    },
    {
        name: 'create',
        path: '/create',
        component: CreateComponent
    },
    {
        name: 'posts',
        path: '/posts',
        component: IndexComponent
    },
    {
        name: 'edit',
        path: 'edit/:id',
        component: EditComponent
    }
];

export default routes;
