<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PostCollection;
use App\Post;

class PostController extends Controller
{

    public function index()
    {
        return new PostCollection(Post::all());
    }
   
    public function store(Request $request) {
        $request->validate([
            'body' => 'required',
            'title' => 'required',
            'auther' => 'required',
        ]);

        $post = Post::create([
            'title' => $request->get('title'),
            'body' => $request->get('body'),
            'auther' => $request->get('auther')
        ]);

        return response()->json(['status' => 'success', 'message' => 'Post Created Successfully.']);
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return response()->json($post);
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'auther' => 'required',
        ]);
        
        $post = Post::find($id);
        $post->update(array(
            'title' => $request->get('title'),
            'body' => $request->get('body'),
            'auther' => $request->get('auther')
        ));

        return response()->json(array('status'=> 'succcess', 'message' => 'Updated Successfully'));
    }

    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();
        return response()->json(array('status'=> 'succcess', 'message' => 'successfully deleted'));
    }
}
